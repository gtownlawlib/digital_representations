# Using the Alma API, create / update digital representations for all of our lending items

from pathlib import Path
import requests
import json
from urlpath import URL
import pyodbc
import pandas as pd
from time import sleep

# read the API key
api_key = Path('../credentials/alma_write_key.txt').read_text().strip()

# define headers for API requests:
headers = {'Content-Type': 'application/json'}
headersxml = {'Content-Type': 'application/xml'}

# define API URIs and params to send with the request:
baseurl = URL('https://api-na.hosted.exlibrisgroup.com/almaws/v1/bibs/')
params = { 'apikey': api_key,  # these are standard API params
           'format': 'json'}

pid = '81186708610004113'  # digital repository id code for our CDL collection (called a PID in Alma)

dsn = 'DSN=digdb-prod;Trusted_Connection=Yes;APP=Python;DATABASE=digitization'  # database access info

# get a list of uids (and associated volume info) from database

cnxn = pyodbc.connect(dsn)
# I could fix up this sql to check the Bib Table and make sure it's not a non-Alma Bib ID.
sql = '''SELECT `ia`.IA_identifier, 
                         `i`.Volume,
                         `ia`.Loan_status,
                         `i`.`Bib_ID`
                FROM Internet_Archive `ia` INNER JOIN Item_Entry `i` ON `ia`.IA_identifier = `i`.UID
                WHERE `ia`.Lending_PDF = True;'''    

df = pd.read_sql(sql, cnxn)  # this dataframe has the info we need to work with

sql = 'SELECT * FROM digital_representations'
reps_df = pd.read_sql(sql, cnxn).set_index('UID')   # a dataframe of the digireps table

multi_items = dict()  # keep track of whether we've done multivolume sets or not so I can simplify the language of 
                      # added volumes -- BUT THIS ISN'T WORKING RIGHT. It's ok I can do a multi-volume cleanup routine later

for row in df.sort_values(by=['IA_identifier']).itertuples():
    if row.IA_identifier in reps_df.index:
        # print(f'{row.IA_identifier} already has a representation.')
        pass
    elif not(row.Bib_ID.endswith('4113')):
        print(f'{row.Bib_ID} is not a valid MMSID.')
    else:
        # add the bib record to the collection
        bib_xml = '<bib><mms_id>' + str(row.Bib_ID) + '</mms_id></bib>'
        uri = baseurl / 'collections' / pid / 'bibs'
        r = requests.post(uri, data=bib_xml, params=params, headers=headersxml)
        add = False
        if r.status_code == 400:
            if json.loads(r.text)['errorList']['error'][0]['errorMessage'] == 'Bib record is already assigned to this collection.':
                print(f'(Bib record for {row.IA_identifier} already in collection.)')
                add = True
            else:
                print(row.IA_identifier)
                print(r.text)
        elif r.status_code == 200:
            add = True
        if add is True:
            sleep(4)
            # if that worked, build up a digital representation object
            rep_object = {
                        "library": {"value": "GUL"},
                        "is_remote": True,
                        "usage_type": {"value": "DERIVATIVE_COPY"},
                        "label": "Check availability",
                        "public_note": "Create your own <a href=\"https://archive.org/account/login.createaccount.php\">free account at archive.org</a> to borrow from Georgetown Law's Controlled Digital Lending collection.<br>Digitized books may be borrowed for 14 days.",
                        "active": {"value": "true"},
                        "repository": {"value": "IA-1"},
                        "originating_record_id": row.IA_identifier,
                        "linking_parameter_1": row.IA_identifier,
                        }

            if row.Volume:
                print(f'* Identified volume data: {row.Volume} in {row.IA_identifier}.')
                prefix = ''
                if row.Volume[0].isdigit():
                    prefix = 'v. '
                if len(row.Volume) == 4 and (row.Volume.startswith('199') or row.Volume.startswith('200') or row.Volume.startswith('201')):
                    prefix = ''
                rep_object['entity_type'] = {'value': 'ISSUE'}
                rep_object['volume'] = prefix + row.Volume
                rep_object['label'] = prefix + row.Volume + ": Check availability"
                if row.IA_identifier in multi_items:  # this isn't working
                    rep_object.pop('public_note')
                else:
                    multi_items[row.IA_identifier] = True
        # ok so let's try to add it
        if rep_object:
            uri = baseurl / row.Bib_ID / 'representations'
            r = requests.post(uri, data=json.dumps(rep_object), params=params, headers=headers)
            if r.status_code == 200:
                new_rep_id = json.loads(r.text)['id']
                print(f'Added {row.IA_identifier}.')
                # print(new_rep_id)  # but really I will want to get the representation ID number
                # ADD TO DATABASE
                # NB later I can deal with availability stuff...  
                cursor = cnxn.cursor()
                cursor.execute('''INSERT INTO digital_representations(UID, MMS_ID, Representation_ID, Label)
                    VALUES (?, ?, ?, ?)''', (row.IA_identifier, row.Bib_ID, new_rep_id, rep_object['label']))
                cursor.commit()
            